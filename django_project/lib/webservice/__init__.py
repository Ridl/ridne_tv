from hashlib import md5

from .decorators import decompress, to_response
from .requests import StreamRequest, StatisticsRequest, MetadataRequest
from suds.client import Client
from suds.xsd.doctor import ImportDoctor, Import


class Credentials(object):
    '''
       Used to authenticate in the CDN
    '''
    def __init__(self, partner_id, private_key):
        self.partner_id = partner_id
        self.private_key = private_key


class Webservice(object):
    def __init__(self, **kwargs):
        if not 'endpoint' in kwargs:
            raise TypeError('You must suply the \'endpoint\' for the webservice')
        elif not isinstance(kwargs['endpoint'], str):
            raise TypeError('\'endpoint\' must be a string')
        else:
            self.endpoint = kwargs['endpoint']
        if (not 'credentials' in kwargs)\
          or (not isinstance(kwargs['credentials'], Credentials)):
            raise TypeError('You must suply \'credentials\' and it must be an instance of \'Credentials\'')
        else:
            self.credentials = kwargs['credentials']
            
        self._client = None
        
    def get_service_client(self):
        if self._client is None:
            self._client = Client(self.endpoint)
        return self._client
    client = property(get_service_client)
        
        
    def _gen_hash(self,*args):
        ars = list((self.credentials.private_key,))
        ars.extend(args)
        st = ''.join([str(x).lower() for x in ars])
        st = md5(st).hexdigest()
        return st


class MetaDataWebservice(Webservice):
    def __init__(self, **kwargs):
        super(MetaDataWebservice,self).__init__(**kwargs)
        if (not 'compression' in kwargs)\
          or (not isinstance(kwargs['compression'],str)):
            self.compression = ''
        else:
            self.compression = kwargs['compression']

        self._cont_max_batch = None
        self._obj_max_batch = None
            
    def container_batch_size(self):
        if not self._cont_max_batch:
            self._cont_max_batch = self.client.service.get_max_batch_size_mediacontainer()
        return self._cont_max_batch
     
    def object_batch_size(self):
        if not self._obj_max_batch:
            self._obj_max_batch = self.client.service.get_max_batch_size_mediaobject()
        return self._obj_max_batch
    
    def get_service_client(self):
        if self._client is None:
            imp = Import('http://www.w3.org/2001/XMLSchema', location='http://www.w3.org/2001/XMLSchema.xsd')
            imp.filter.add('http://matvil.com/')
            d = ImportDoctor(imp)
            self._client = Client(self.endpoint,doctor=d)
        return self._client
    client = property(get_service_client)
    
    @to_response
    @decompress
    def category_list(self, category_type):
        partnerId = self.credentials.partner_id
        hashParam = self._gen_hash(category_type,self.compression)
        return self.client.service.get_media_category_list(partnerId,hashParam,category_type,self.compression)
    
    @to_response
    @decompress
    def category_list_by_media_id(self, media_id, media_type, category_type):
        partnerId = self.credentials.partner_id
        hashParam = self._gen_hash(category_type, media_type, media_id, self.compression)
        return self.client.service.get_media_category_list_by_media_id(partnerId,hashParam,category_type,media_type,media_id,self.compression)
    
    @to_response
    @decompress
    def media_by_id(self, media_id,media_type='media_object',include_related=False):
        partnerId = self.credentials.partner_id
        
        hashParam = self._gen_hash(media_type, media_id,include_related and 'true' or 'false', self.compression)
        return self.client.service.get_media_by_id(partnerId,hashParam,media_type,media_id,include_related,self.compression)
    
    #@to_response
    #@decompress
    def object_list(self, last_updated=0, offset=1,batchsize=None, include_related='true'):
        if not batchsize:
            batchsize = self.container_batch_size()
        partnerId = self.credentials.partner_id
        
        req = MetadataRequest('audio',last_updated,int(batchsize),include_related)

        hashParam = self._gen_hash(str(req))
        return self.client.service.get_media_object_list(partnerId,hashParam,str(req))
    
    @to_response
    @decompress
    def object_list_2(self, maxChangeId=1, batchsize=None, include_related=False):
        if not batchsize:
            batchsize = self.container_batch_size()
        partnerId = self.credentials.partner_id
        
        hashParam = self._gen_hash(maxChangeId, batchsize,include_related and 'true' or 'false', self.compression)
        return self.client.service.get_media_object_list_2(partnerId,hashParam,maxChangeId,batchsize,include_related,self.compression)
    
    @to_response
    @decompress
    def container_list(self, last_updated='', offset=1, batchsize=None, include_related=False):
        if not batchsize:
            batchsize = self.container_batch_size()
        partnerId = self.credentials.partner_id

        hashParam = self._gen_hash(last_updated, offset, batchsize,include_related and 'true' or 'false', self.compression)
        return self.client.service.get_media_container_list(partnerId,hashParam,last_updated,offset,batchsize,include_related,self.compression)
    
    @to_response
    @decompress
    def container_list_2(self,maxChangeId=1,batchsize=None, include_related=False):
        if not batchsize:
            batchsize = self.container_batch_size()
        partnerId = self.credentials.partner_id

        hashParam = self._gen_hash(maxChangeId, batchsize,include_related and 'true' or 'false', self.compression)
        return self.client.service.get_media_container_list_2(partnerId,hashParam,maxChangeId,batchsize,include_related,self.compression)
    
    @to_response
    @decompress
    def person_list_by_media_id(self, media_id, media_type):
        partnerId = self.credentials.partner_id
        hashParam = self._gen_hash(media_type, media_id, self.compression)
        return self.client.service.get_media_person_list_by_media_id(partnerId,hashParam,media_type,media_id,self.compression)

    @to_response
    @decompress
    def media_country_list(self):
        partnerId = self.credentials.partner_id
        hashParam = self._gen_hash(self.compression)
        return self.client.service.get_media_country_list(partnerId,hashParam,self.compression)

    @to_response
    @decompress
    def media_channel_list(self):
        partnerId = self.credentials.partner_id
        hashParam = self._gen_hash(self.compression)
        return self.client.service.get_media_channel_list(partnerId, hashParam, self.compression)


class StreamWebservice(Webservice):
    def __init__(self,**kwargs):
        super(StreamWebservice,self).__init__(**kwargs)

    @to_response
    def get_url(self,user_id,user_ip,media_id,bitrate_id,is_preview=False,max_allowed=1,rule_id=''):
        partnerId = self.credentials.partner_id

        hashParam = self._gen_hash(user_id,user_ip, media_id, bitrate_id,is_preview,max_allowed)
        return self.client.service.get_media_stream_url(partnerId,hashParam,user_id,user_ip,media_id,bitrate_id,is_preview,max_allowed,rule_id)

    @to_response
    def get_url_adv(self,user_id,user_ip,user_plan,media_id,bitrate_id,is_preview=False,max_allowed=1,stb_type='',next_server_mode='',user_agent='',timeshift='',is_premium=False):
        partnerId = self.credentials.partner_id
        req = StreamRequest(user_id, user_ip, user_plan, media_id, bitrate_id, is_preview, max_allowed,stb_type,next_server_mode and 1 or 0,user_agent,timeshift,is_premium)
        strParams = str(req)
        hashParam = self._gen_hash(strParams)
        return self.client.service.get_media_stream_url_adv(partnerId,hashParam,strParams)

    def _gen_hash(self,*args):
        ars = list((self.credentials.private_key,))
        ars.extend(args)
        st = ''.join([str(x) for x in ars])
        st = md5(st).hexdigest()
        return st


class StatisticsWebservice(Webservice):
    def __init__(self, **kwargs):
        super(StatisticsWebservice,self).__init__(**kwargs)
        if (not 'compression' in kwargs)\
          or (not isinstance(kwargs['compression'],str)):
            self.compression = ''
        else:
            self.compression = kwargs['compression']
        
        self._max_batch = None
        
    def max_batch_size(self):
        if not self._max_batch:
            self._max_batch = min(self.client.service.get_max_batch_size_statistics(), 1000)
        return self._max_batch
    
    @to_response
    @decompress    
    def get_statistics(self,maxChangeId,batch_size=None,include_zero_streams=False):
        if batch_size is None:
            batch_size = self.max_batch_size()
        
        partnerId = self.credentials.partner_id
        req = StatisticsRequest(maxChangeId, batch_size, include_zero_streams, self.compression)
        hashParam = self._gen_hash(req)
        return self.client.service.get_statistics(partnerId,hashParam,str(req))
    
