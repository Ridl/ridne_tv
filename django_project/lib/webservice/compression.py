

def decompress(response, method=None):
    '''
        Uncompresses the response if compressed
        Supported methods should be placed to COMPRESS_METHODS dictionary
    '''
    if not method:
        return response
    from base64 import b64decode
    try:
        response = b64decode(response)
    except:
        return None
    if method in COMPRESS_METHODS:
        return COMPRESS_METHODS[method](response)
    else:
        raise NotImplementedError('\'%s\' compression method is not supported')


def __ungzip(response):
    from cStringIO import StringIO
    from gzip import GzipFile
    s = StringIO(response)
    try:
        gzipfile = GzipFile(fileobj=s)
        response = gzipfile.read()
    except:
        response = None
    finally:
        return response

COMPRESS_METHODS = {'gzip':__ungzip}
