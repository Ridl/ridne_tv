from .compression import decompress as _decompress
from .responses import *
try:
    from functools import wraps
except ImportError:
    from django.utils.functional import wraps
    
RESPONSES = {
        'category_list':Response,
        'category_list_by_media_id':Response,
        'person_list_by_media_id':Response,
        'object_list':MediaResponse,
        'object_list_2':MediaResponse2,
        'media_by_id':Response,
        'container_list':MediaResponse,
        'container_list_2':MediaResponse2,
        'get_url' : StreamResponse,
        'get_url_adv' : StreamResponse,
        'get_statistics':Response,
        'get_statistics_by_client':Response,
        'get_statistics_by_client_active_only':Response,
        'media_country_list': Response,
        'media_channel_list': Response,
    }
    
def decompress(func):
    def inner(instance, *args, **kwargs):
        result = func(instance, *args, **kwargs)
        return _decompress(result, instance.compression)
        
    return wraps(func)(inner)

def to_response(func):
    def inner(instance, *args, **kwargs):
        result = func(instance, *args, **kwargs)
        return RESPONSES[func.__name__](result)
        
    return wraps(func)(inner)
