import time

class MetadataRequest(object):
    XML_TEMPLATE ='<root>'\
'<mediaType>%s</mediaType>'\
'<maxChangeId>%s</maxChangeId>'\
'<batchSize>%d</batchSize>'\
'<includeRelatedTables>%s</includeRelatedTables>'\
'</root>'

    def __init__(self, media_type,max_change_id,batch_size,include_related):
        self.media_type = media_type
        self.max_change_id = max_change_id
        self.batch_size = batch_size
        self.include_related = include_related
    
    def __repr__(self):
        return self.XML_TEMPLATE % (
                self.media_type,
                self.max_change_id,
                self.batch_size,
                self.include_related
            )


class StreamRequest(object):
    XML_TEMPLATE ='''<root>
<partnerClientId>%s</partnerClientId>
<clientIPaddress>%s</clientIPaddress>
<mediaId>%d</mediaId>
<bitrateId>%d</bitrateId>
<isPreview>%s</isPreview>
<clientMaxActiveStreamQty>%d</clientMaxActiveStreamQty>
<mediaPacketId>%s</mediaPacketId>
<stbType>%s</stbType>
<nextServerMode>%s</nextServerMode>
<userAgent>%s</userAgent>
<created_ts>%s</created_ts>
<isPremium>%s</isPremium>
<place>utils.cdn</place>
%s
</root>'''
    def __init__(self, user_id,user_ip,user_plan,media_id, bitrate_id=600,is_preview=False,max_allowed=1,stb_type='',next_server_mode='',user_agent='',timeshift='', isPremium=False):
        self.user_id = user_id
        self.user_ip = user_ip
        self.user_plan = user_plan
        self.media_id = media_id
        self.bitrate_id = bitrate_id
        self.is_preview = is_preview
        self.max_allowed = max_allowed
        self.stb_type = stb_type
        self.next_server_mode = next_server_mode
        self.user_agent = user_agent
        if isinstance(timeshift,(int,long)):
            self.timeshift = '<utcOffsetSec>%s</utcOffsetSec>'%timeshift
        else:
            self.timeshift = ''
        self.isPremium=isPremium
    
    def __str__(self):
        return self.XML_TEMPLATE % (
                self.user_id, self.user_ip,
                self.media_id, self.bitrate_id,
                self.is_preview and 'true' or 'false',
                self.max_allowed,
                self.user_plan, self.stb_type,
                self.next_server_mode, self.user_agent,
                time.time(),bool(self.isPremium),self.timeshift
            )

class StatisticsRequest(object):
    XML_TEMPLATE ='''<root>
<maxChangeId>%s</maxChangeId>
<batchSize>%s</batchSize>
<includeZeroStreams>%s</includeZeroStreams>
<includeIp>%s</includeIp>
<compressAlgorithm>%s</compressAlgorithm>
</root>'''
    def __init__(self,maxChangeId,batch_size,include_zero_streams=False,compression='',include_ip=True):
        self.maxChangeId = maxChangeId
        self.batch_size = batch_size
        self.include_zero_streams = include_zero_streams
        self.compression = compression
        self.include_ip = include_ip
        
    def __repr__(self):
        return self.XML_TEMPLATE % (
                self.maxChangeId, self.batch_size,
                self.include_zero_streams and 'true' or 'false',
                self.include_ip and 'true' or 'false',
                self.compression,
            )
