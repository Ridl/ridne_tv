class XmlListConfig(list):
    def __init__(self, aList):
        for element in aList:
            if element:
                # treat like dict
                if len(element) == 1 or element[0].tag != element[1].tag:
                    self.append(XmlDictConfig(element))
                # treat like list
                elif element[0].tag == element[1].tag:
                    self.append(XmlListConfig(element))
            elif element.text:
                text = element.text.strip()
                if text:
                    self.append(text)


class XmlDictConfig(dict):
    '''
    Example usage:
    >>> tree = ElementTree.parse('your_file.xml')
    >>> root = tree.getroot()
    >>> xmldict = XmlDictConfig(root)
    Or, if you want to use an XML string:
    >>> root = ElementTree.XML(xml_string)
    >>> xmldict = XmlDictConfig(root)
    And then use xmldict for what it is... a dict.
    '''
    def __init__(self, parent_element):
        if parent_element.items():
            self.update(dict(parent_element.items()))
        for element in parent_element:
            if element:
                # treat like dict - we assume that if the first two tags
                # in a series are different, then they are all different.
                if len(element) == 1 or element[0].tag != element[1].tag:
                    aDict = XmlDictConfig(element)
                # treat like list - we assume that if the first two tags
                # in a series are the same, then the rest are the same.
                else:
                    # here, we put the list in dictionary; the key is the
                    # tag name the list elements all share in common, and
                    # the value is the list itself
                    aDict = {element[0].tag: XmlListConfig(element)}
                # if the tag has attributes, add those to the dict
                if element.items():
                    aDict.update(dict(element.items()))
                self.update({element.tag: aDict})
            # this assumes that if you've got an attribute in a tag,
            # you won't be having any text. This may or may not be a
            # good idea -- time will tell. It works for the way we are
            # currently doing XML configuration files...
            elif element.items():
                self.update({element.tag: dict(element.items())})
            # finally, if there are no child tags and no attributes, extract
            # the text
            else:
                self.update({element.tag: element.text})


from xml.etree import cElementTree as ElementTree


class XmlToDict(object):
    def __call__(self, xml):
        try:
            try:
                xml = xml.encode('utf8')
            except:
                pass
            root = ElementTree.fromstring(xml)
            return XmlDictConfig(root)
        except:
            return None


class Response(object):
    def __init__(self, xml):
        xml_to_dict = XmlToDict()
        xmldict = xml_to_dict(xml)
        self._init(xmldict)
        
    def _init(self, xmldict):
        if (not xmldict)\
          or (not 'status' in xmldict):
            self._invalid_response()
        self.status = xmldict['status']

        if self.status == 'ok' and (not 'received_rows' in xmldict):
            self._invalid_response()
        elif self.status == 'error' and 'error_message' in xmldict:
            self.error_message = xmldict['error_message']
            return

        if self.status == 'ok':
            self.received_rows = int(xmldict['received_rows'])
        
        if self.status == 'ok' and self.received_rows > 0:
            try:
                self.objects = xmldict['ws_result'].items()[0][1]
                if not isinstance(self.objects, list):
                    self.objects=[self.objects]
            except:
                self._invalid_response()
    
    def __getitem__(self,key):
        return self.objects[key]
    
    def __setitem__(self,key, value):
        self.objects[key] = value
    
    def _invalid_response(self):
        '''
           Raises TypeError
        '''
        raise TypeError('Invalid response received')
    
    def __error_occured(self, message):
        '''
           Raises TypeError
        '''
        raise TypeError('An error occured: %s' % (message,))

class MediaResponse(Response):
    def _init(self, xmldict):
        super(MediaResponse, self)._init(xmldict)
        if self.status == 'ok':
            if (not 'batch_first_row_index' in xmldict)\
            or (not 'batch_last_row_index' in xmldict)\
            or (not 'max_batch_size' in xmldict)\
            or (not 'total_rows' in xmldict)\
            or (not 'ws_result' in xmldict):
                self._invalid_response()
            else:
                self.first_row_number = int(xmldict['batch_first_row_index'])
                self.last_row_number = int(xmldict['batch_last_row_index'])
                self.total_rows = int(xmldict['total_rows'])
                
class MediaResponse2(Response):
    def _init(self,xmldict):
        super(MediaResponse2,self)._init(xmldict)
        if self.status == 'ok':
            if (not 'batch_size' in xmldict)\
            or (not 'total_rows' in xmldict)\
            or (not 'ws_result' in xmldict):
                self._invalid_response()
            else:
                self.batch_size = int(xmldict['batch_size'])
                self.total_rows = int(xmldict['total_rows'])
            
class StreamResponse(object):
    def __init__(self, xml):
        xml_to_dict = XmlToDict()
        xmldict = xml_to_dict(xml)
        self._init(xmldict)
        
    def _init(self, xmldict):
        if (not xmldict)\
          or (not 'status' in xmldict):
            self.__invalid_response()
        self.status = xmldict['status']

        if self.status == 'error' and 'error_message' in xmldict:
            self.error_message = xmldict['error_message']
            return
        
        if 'stream_url' in xmldict and 'stream_id' in xmldict:
            self.stream_url = xmldict['stream_url']
            if self.stream_url.startswith('http://wss.mtvl.xc.advection.net'):
                self.stream_url = '%s%s' % ('mms://',self.stream_url[7:])
            self.stream_id = xmldict['stream_id']
        else:
            self.__invalid_response()
            
    def __invalid_response(self):
        '''
           Raises TypeError
        '''
        raise TypeError('Invalid response received')
