var app = angular.module('ridneApp', ['ui.router'] );


    app.config(function($stateProvider, $urlRouterProvider, $locationProvider){

    // For any unmatched url, send to /route1
    //$urlRouterProvider.otherwise("/");

    $stateProvider
        .state('main-page', {
            url: "/",
            views: {
                "sliderView": { templateUrl: "/static/view/slider.html", template: '<movie-slider></movie-slider>'},
                "movieTop":{ templateUrl: "/static/view/movielisting.html", template: '<div class="movie-best"> <div class="col-sm-10 col-sm-offset-1 movie-best__rating">Краще за сьогодні</div> <div class="col-sm-12 change--col"> <movie-top info="top" ng-repeat="top in tops"></movie-top> </div> <div class="col-sm-10 col-sm-offset-1 movie-best__check">check all movies now playing</div></div>' },

                "mainContentView": { templateUrl: "/static/view/main-page.html"}
            }
        })
        .state('full-movie-list', {
            url: "/full-movie-list",
            views: {
                "mainContentView": { templateUrl: "/static/view/full-movie-list.html" }
                //"searchView": { templateUrl: "view/search.html" }
            }
        })
        .state('movie-page', {
            url: "/movie/",
            views: {
                "mainContentView": { templateUrl: "/static/view/movie-page-left.html" }
            }
        })
        .state('news-page', {
            url: "/news",
            views: {
                "mainContentView": { templateUrl: "/static/templates/view/news" }



            }
        });
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });



});

app.config(['$urlRouterProvider',function($urlRouterProvider){
    $urlRouterProvider.otherwise('/');
  }]);







