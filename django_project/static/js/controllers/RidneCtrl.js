app.controller('ridneControl', ['$scope', function($scope) {

    $scope.tops = [
        {
            image: 'static/images/movie/movie-sample1.jpeg',
            rate: '5.0',
            voted: '180 переглядів',
            gene: 'Пригодницький | Драма | Фентезі'

        },
        {
            image: 'static/images/movie/movie-sample2.jpg',
            rate: '5.0',
            voted: '180 переглядів',
            gene: 'Пригодницький | Драма | Фентезі'

        },
        {
            image: 'static/images/movie/movie-sample3.jpg',
            rate: '5.0',
            voted: '180 переглядів',
            gene: 'Пригодницький | Драма | Фентезі'

        },
        {
            image: 'static/images/movie/movie-sample4.jpg',
            rate: '5.0',
            voted: '180 переглядів',
            gene: 'Пригодницький | Драма | Фентезі'

        },
        {
            image: 'static/images/movie/movie-sample5.jpg',
            rate: '5.0',
            voted: '180 переглядів',
            gene: 'Пригодницький | Драма | Фентезі'

        },
        {
            image: 'static/images/movie/movie-sample6.jpg',
            rate: '5.0',
            voted: '180 переглядів',
            gene: 'Пригодницький | Драма | Фентезі'

        }
    ];
    $scope.moviefronts = [
        {
            movie_images: 'static/images/424.png',
            movie_title: 'Gravity (2013)',
            movie_time: '91 min',
            movie_rating: '4.4',
            movie_option: ''
        },
        {
            movie_images: 'static/images/424.png',
            movie_title: 'Gravity (2013)',
            movie_time: '91 min',
            movie_rating: '4.6',
            movie_option: ''
        },
        {
            movie_images: 'static/images/424.png',
            movie_title: 'Gravity (2013)',
            movie_time: '91 min',
            movie_rating: '5.0',
            movie_option: ''
        },
        {
            movie_images: 'static/images/424.png',
            movie_title: 'Gravity (2013)',
            movie_time: '91 min',
            movie_rating: '4.9',
            movie_option: ''
        }, {
            movie_images: 'static/images/424.png',
            movie_title: 'Gravity (2013)',
            movie_time: '91 min',
            movie_rating: '4.1',
            movie_option: ''
        },
        {
            movie_images: 'static/images/424.png',
            movie_title: 'Gravity (2013)',
            movie_time: '91 min',
            movie_rating: '3.8',
            movie_option: ''
        }
    ];


    $scope.tvboxs = [
        {
            movie_images: 'static/images/tv/1plus1.jpg'

        },
        {
            movie_images: 'static/images/tv/inter.jpg'

        },
        {
            movie_images: 'static/images/tv/ntn.jpg'

        },
        {
            movie_images: 'static/images/tv/k1.jpg'

        },
        {
            movie_images: 'static/images/tv/k2.jpg'

        },
        {
            movie_images: 'static/images/tv/mega.jpg'

        },
        {
            movie_images: 'static/images/tv/stb.jpg'

        },
        {
            movie_images: 'static/images/tv/1tv-ua.jpg'

        },
        {
            movie_images: 'static/images/tv/2plus2.jpg'

        },
        {
            movie_images: 'static/images/tv/ictv.jpg'

        },
        {
            movie_images: 'static/images/tv/noviy-tv.jpg'

        },
        {
            movie_images: 'static/images/tv/gamma.jpg'

        },
        {
            movie_images: 'static/images/tv/business-tv.jpg'

        },
        {
            movie_images: 'static/images/tv/ukraina-tv.jpg'

        },
        {
            movie_images: 'static/images/tv/tonis.jpg'

        },
        {
            movie_images: 'static/images/tv/pixel-tv.jpg'

        },
        {
            movie_images: 'static/images/tv/zik-tv.jpg'

        },
        {
            movie_images: 'static/images/tv/24tv.jpg'

        },
        {
            movie_images: 'static/images/tv/112tv.jpg'

        },
        {
            movie_images: 'static/images/tv/newsone.jpg'

        },
        {
            movie_images: 'static/images/tv/hromadske-tv.jpg'

        },
        {
            movie_images: 'static/images/tv/espreso-tv.jpg'

        },
        {
            movie_images: 'static/images/tv/m1.jpg'

        },
        {
            movie_images: 'static/images/tv/m2.jpg'

        },
        {
            movie_images: 'static/images/tv/enter-film.jpg'

        },
        {
            movie_images: 'static/images/tv/zoom-tv.jpg'

        },
        {
            movie_images: 'static/images/tv/football1.jpg'

        },
        {
            movie_images: 'static/images/tv/football2.jpg'

        },
        {
            movie_images: 'static/images/tv/tet.jpg'

        },
        {
            movie_images: 'static/images/tv/qtv.jpg'

        }
        //{
        //    movie_images:'images/tv/a-one-ua.jpg'
        //
        //}
        //{
        //    movie_images:'images/tv/plus-plus.jpg'
        //
        //},
        //{
        //    movie_images:'images/tv/sport1.jpg'
        //
        //},
        //{
        //    movie_images:'images/tv/1tv.jpg'
        //
        //}

    ];
    $scope.movielisings = [
        {
            movie_tittle: 'Хоббіт: Несподівана подорож',
            movie_image: '/static/images/movie/hobbit.jpg',
            movie_rate: '4.8',
            movie_time: '165 min',
            movie_country: 'Нова Зеландія, США',
            movie_year: '2012',
            movie_category: 'Пригодницький, Фентезі',
            movie_release: '12.12.2012',
            movie_director: 'Пітер Джексон',
            movie_actors: 'Мартін Фріман, Бенедикт Камбербетч, Іен Маккеллен, Хюго Вівінг, Еванджелін Ліллі, Люк Еванс, Кейт Бланшетт, Елайджа Вуд, Крістофер Лі, Орландо Блум, Іен Маккеллен',
            movie_age: '13',
            movie_office: '$1 017 003 568',
            movie_comment: '15',
            movie_description: 'Екранізація повісті Джона Толкіна «Хоббіт, або Туди і назад», що оповідає про Більбо Беггінса, який відправився з компанією гномів у велику і небезпечну подорож, щоб визволити Королівство Гномів, захоплене драконом Смогом ...'


        }
    ];


}]);







//<div class="movie__rate">Your vote: <div id='score' class="score"></div></div>
//</div>
//
//<div class="col-sm-8 col-md-9">
//    <p class="movie__time">169 min</p>
//
//<p class="movie__option"><strong>Country: </strong><a href="#">New Zeland</a>, <a href="#">USA</a></p>
//<p class="movie__option"><strong>Year: </strong><a href="#">2012</a></p>
//<p class="movie__option"><strong>Category: </strong><a href="#">Adventure</a>, <a href="#">Fantazy</a></p>
//<p class="movie__option"><strong>Release date: </strong>December 12, 2012</p>
//<p class="movie__option"><strong>Director: </strong><a href="#">Peter Jackson</a></p>
//<p class="movie__option"><strong>Actors: </strong><a href="#">Martin Freeman</a>, <a href="#">Ian McKellen</a>, <a href="#">Richard Armitage</a>, <a href="#">Ken Stott</a>, <a href="#">Graham McTavish</a>, <a href="#">Cate Blanchett</a>, <a href="#">Hugo Weaving</a>, <a href="#">Ian Holm</a>, <a href="#">Elijah Wood</a> <a href="#">...</a></p>
//<p class="movie__option"><strong>Age restriction: </strong><a href="#">13</a></p>
//<p class="movie__option"><strong>Box office: </strong><a href="#">$1 017 003 568</a></p>
//
//<a href="#" class="comment-link">Comments:  15</a>




