app.directive("fullMovie", function() {
    return{
        restrict: 'E',
        scope:{
            info: '='
        },
        templateUrl: '/static/view/movieList.html'
    }
});