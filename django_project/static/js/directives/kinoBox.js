app.directive("kinoBox", function() {
    return{
        restrict: 'E',
        scope:{
            info: '='
        },
        templateUrl: '/static/view/kinoBox.html'
    }
});