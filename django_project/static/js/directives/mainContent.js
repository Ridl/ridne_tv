app.directive("mainContent", function() {
    return{
        restrict: 'E',
        scope:{
            info: '='
        },
        templateUrl: '/static/view/main-page.html'

    }
});