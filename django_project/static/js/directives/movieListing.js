app.directive("movieTop", function() {
    return{
        restrict: 'E',
        scope:{
            info: '='
        },
        templateUrl: '/static/view/movieListing.html'
    }
});