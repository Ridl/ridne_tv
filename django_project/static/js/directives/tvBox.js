app.directive("tvBox", function() {
    return{
        restrict: 'E',
        scope:{
            info: '='
        },
        templateUrl: '/static/view/tvBox.html'
    }
});