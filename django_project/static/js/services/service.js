app.factory('shows', ['$http', function($http) {
  return $http.get('/api/rubrics/')
            .success(function(data) {
              return data;
            })
            .error(function(err) {
              return err;
            });

}]);