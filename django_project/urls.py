# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from django.views import defaults as default_views
from rest_framework import routers
from media import views

admin.autodiscover()
router = routers.DefaultRouter()
router.register(r'medias', views.MediaViewSet)
router.register(r'rubrics', views.RubricViewSet)
router.register(r'countries', views.MediaCountryViewSet)
router.register(r'channels', views.ChannelViewSet)


urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='pages/home.html'), name="home"),
    url(r'^about/$', TemplateView.as_view(template_name='pages/../django_project/static/view/about.html'), name="about"),

    # Django Admin, use {% url 'admin:index' %}
    # url(settings.ADMIN_URL, include(admin.site.urls)),
    url(r'^admin/', include(admin.site.urls)),

    # # User management
    # url(r'^users/', include("django_project.users.urls", namespace="users")),
    # url(r'^accounts/', include('allauth.urls')),

    # Your stuff: custom urls includes go here

    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))


]
