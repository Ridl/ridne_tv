from django.contrib import admin
from models import (
    Genre,
    Media,
)


class MediaAdmin(admin.ModelAdmin):
    list_display = ('name', 'parent')
    pass
admin.site.register(Media)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name',)
    pass
admin.site.register(Genre)

