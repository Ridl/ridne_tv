from django.core.management.base import BaseCommand, CommandError
from django_project.lib.webservice import MetaDataWebservice, Credentials
from media.models import Rubric
from django.conf import settings

cred = Credentials(settings.CDN_PARTNER_ID, settings.CDN_PRIVATE_KEY)
endpoint = settings.CDN_ENDPOINT


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        pass
        # parser.add_argument('poll_id', nargs='+', type=int)

    def handle(self, *args, **options):

        service = MetaDataWebservice(endpoint=endpoint, credentials=cred)

        self.response = service.category_list('category_common')

        if self.response.status == 'ok':
            self.stdout.write(self.style.SUCCESS('SAVING CATEGRIES ... '))
            i = 0
            while i < self.response.received_rows:
                cur_item = self.response[i]
                self.create_category_item(cur_item)
                i += 1
        else:
            self.stdout.write(self.style.ERROR('CDN ERROR WHEN GETTING CATEGORIES'))

    def create_category_item(self, cur_item):
        item = {'name': cur_item['name']}
        if 'category_order' in cur_item:
            item['order'] = cur_item['category_order']
        if 'parent_category_id' in cur_item:
            try:
                item['parent'] = Rubric.objects.get(external_id=cur_item['parent_category_id'])
            except Rubric.DoesNotExist:
                for parent_item in self.response:
                    if parent_item['id'] == cur_item['parent_category_id']:
                        item['parent'] = self.create_category_item(parent_item)

        rubric, created = Rubric.objects.get_or_create(external_id=cur_item['id'], defaults=item)
        if not created:
            for k, v in item.items():
                setattr(rubric, k, v)
        rubric.save()
        return rubric



