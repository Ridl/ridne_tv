from django.core.management.base import BaseCommand, CommandError
from django_project.lib.webservice import MetaDataWebservice, Credentials
from media.models import Channel
from django.conf import settings
from django.db.utils import IntegrityError

cred = Credentials(settings.CDN_PARTNER_ID, settings.CDN_PRIVATE_KEY)
endpoint = settings.CDN_ENDPOINT


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        pass
        # parser.add_argument('poll_id', nargs='+', type=int)

    def handle(self, *args, **options):

        service = MetaDataWebservice(endpoint=endpoint, credentials=cred)

        self.response = service.media_channel_list()

        if self.response.status == 'ok':
            self.stdout.write(self.style.SUCCESS('SAVING CHANNELS ... '))
            i = 0
            while i < self.response.received_rows:
                cur_item = self.response[i]
                self.create_channel_item(cur_item)
                i += 1
        else:
            self.stdout.write(self.style.ERROR('CDN ERROR WHEN GETTING CHANNELS'))

    def create_channel_item(self, channel):
        defaults = {
            'id': int(channel['id']),
            'name': channel['name'],
            'order': int(channel['channel_order']),
            'hidden': channel['active'] == 'false',
            'cdn_id': channel['cdn_id']
        }

        try:
            Channel.objects.update_or_create(id=int(channel['id']), defaults=defaults)
        except IntegrityError:
            pass



