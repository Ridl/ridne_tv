from django.core.management.base import BaseCommand, CommandError
from django_project.lib.webservice import MetaDataWebservice, Credentials
from media.models import MediaCountry
from django.conf import settings

cred = Credentials(settings.CDN_PARTNER_ID, settings.CDN_PRIVATE_KEY)
endpoint = settings.CDN_ENDPOINT


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        pass
        # parser.add_argument('poll_id', nargs='+', type=int)

    def handle(self, *args, **options):

        service = MetaDataWebservice(endpoint=endpoint, credentials=cred)

        self.response = service.media_country_list()

        if self.response.status == 'ok':
            self.stdout.write(self.style.SUCCESS('SAVING COUNTRIES ... '))
            i = 0
            while i < self.response.received_rows:
                cur_item = self.response[i]
                self.create_country_item(cur_item)
                i += 1
        else:
            self.stdout.write(self.style.ERROR('CDN ERROR WHEN GETTING COUNTRIES'))

    def create_country_item(self, country):
        defaults = {'title': country['name'], 'active': True if country['active'].lower() == 'true' else False}
        c, created = MediaCountry.objects.update_or_create(id=country['id'], defaults=defaults)
        print c



