from django.core.management.base import BaseCommand, CommandError
from django_project.lib.webservice import MetaDataWebservice, Credentials
from .save_to_db import save_media
from django.conf import settings


cred = Credentials(settings.CDN_PARTNER_ID, settings.CDN_PRIVATE_KEY)
endpoint = settings.CDN_ENDPOINT


def setMaxChangeId(maxChangeId):
    with open('maxChangeIdObjects.txt', 'w') as f:
        f.write(str(maxChangeId))


def getMaxChangeId():
    try:
        with open('maxChangeIdObjects.txt', 'r') as f:
            maxChangeId = f.read()
        return maxChangeId if maxChangeId else 0
    except IOError:
        setMaxChangeId(0)


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        pass
        # parser.add_argument('poll_id', nargs='+', type=int)

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('RECEIVING MEDIA OBJECTS ... '))
        self.get_objects(maxChangeId=getMaxChangeId())

    def get_objects(self, maxChangeId=None):
        service = MetaDataWebservice(endpoint=endpoint, credentials=cred)
        maxChangeId = getMaxChangeId()
        print 'Starting from maxChangeId =', maxChangeId
        received_rows = 1
        while received_rows > 0:
            response = service.container_list_2(maxChangeId=maxChangeId,
                                                include_related=True,
                                                batchsize=50)
            j = received_rows = 0
            if response.status == 'ok' and response.received_rows > 0 and hasattr(response, 'objects'):
                # from pprint import pprint
                # pprint(response.objects)
                received_rows = response.received_rows
                save_counter = 0
                while j < received_rows:
                    cur_item = response.objects[j]
                    maxChangeId = int(cur_item['updated_ts'])

                    save_counter += 1
                    save_media(cur_item, container=False)

                    j += 1
                    setMaxChangeId(maxChangeId)
                print 'Received:', received_rows, 'Saved:', save_counter, ', Total:', response.total_rows, ', MaxChangeId: ', maxChangeId

