from media.models import Media, Rubric, MediaCountry, Channel
import six


def save_media(item, container=None):
    assert container is not None, 'Please specify is it container or not by setting set`container` to True or False'

    parent = None

    from pprint import pprint
    pprint(item)

    if item.get('container_id'):
        try:
            parent, created = Media.objects.get_or_create(cdn_id=int(item['container_id']))
        except Media.DoesNotExist:
            pass

    channel = item.get('channel_id')
    if channel is not None:
        channel, created = Channel.objects.update_or_create(id=int(channel),
                                                            defaults={
                                                                'name': item.get('channel_name'),
                                                                'cdn_id': channel
                                                            })


    media, created = Media.objects.update_or_create(
        cdn_id=item['id'],
        defaults={
            'is_container': container,
            'name': item['name'] or 'Unknown',
            'description_full': item['description_full'],
            'production_year': item.get('production_year'),
            'parent': parent,
            'channel': channel
        }
    )

    # lets save categories
    categories = item.get('category_commons')
    if categories is not None and 'id' in categories:
        categories = categories['id']
        if not isinstance(categories, list):
            categories = [categories]
        rubrics = Rubric.objects.filter(external_id__in=categories)
        media.rubrics.add(*list(rubrics))

    # lets save countries
    countries = item.get('media_countries')
    if countries is not None and 'id' in countries:
        countries = countries['id']
        if not isinstance(countries, list):
            countries = [countries]
        countries_queryset = MediaCountry.objects.filter(id__in=countries)
        media.production_countries.add(*list(countries_queryset))
