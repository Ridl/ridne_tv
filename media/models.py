from __future__ import unicode_literals

from django.db import models
from django.core.cache import cache


class Category(models.Model):
    external_id = models.PositiveIntegerField()
    name = models.CharField(max_length=100)
    order = models.IntegerField(default=0)
    parent = models.ForeignKey('self', null=True, blank=True, related_name='children')

    type = models.CharField(max_length=100, null=True, blank=True)

    order_api = models.IntegerField(default=0)

    class Meta:
        abstract = True
        ordering = ['-order']

    def __unicode__(self):
        return self.name

    def get_childrens(self):
        result = cache.get('Category:get_childrens:%s' % self.id)
        if not result:
            result = []
            queue = [self.pk]
            while queue:
                queue += list(self.__class__.objects.filter(parent=queue[0]).values_list('id', flat=True))
                result.append(queue[0])
                queue.remove(queue[0])
            cache.set('Category:get_childrens:%s' % self.id, result)
        return result


class Genre(Category):
    def __unicode__(self):
        return self.name


class Theme(Category):
    pass


class Rubric(Category):
    pass


class Channel(models.Model):
    name = models.CharField(max_length=255, unique=True)

    order = models.PositiveIntegerField(default=0)
    hidden = models.BooleanField(default=False)

    cdn_id = models.PositiveIntegerField(default=0)

    commercial_only = models.BooleanField(default=False, help_text=u'Can only be seen by commercial customers')

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('order',)


class MediaCountry(models.Model):
    title = models.CharField(max_length=255)
    active = models.BooleanField(default=False)

    def __unicode__(self):
        return self.title


class Media(models.Model):
    is_container = models.BooleanField(default=True)

    cdn_id = models.PositiveIntegerField()

    channel = models.ForeignKey('Channel', null=True, blank=True)

    name = models.CharField(max_length=255)

    description_short = models.TextField(max_length=500, null=True, blank=True)
    description_full = models.TextField(max_length=1000, null=True, blank=True)

    production_countries = models.ManyToManyField(MediaCountry)

    production = models.CharField(max_length=200, null=True, blank=True)
    production_year = models.PositiveIntegerField(null=True, blank=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, db_index=True)

    genres = models.ManyToManyField(Genre, blank=True)
    themes = models.ManyToManyField(Theme, blank=True)
    rubrics = models.ManyToManyField(Rubric, blank=True)

    active = models.BooleanField(default=False)

    parent = models.ForeignKey('self', null=True, blank=True,  on_delete=models.CASCADE)
    on_air = models.DateTimeField(null=True, blank=True)
    duration = models.PositiveIntegerField(default=0)

    children_count = models.PositiveIntegerField(default=0)

    series_len = models.PositiveIntegerField(null=True, blank=True)
    series_num = models.PositiveIntegerField(default=0, blank=True, db_index=True)

    def __str__(self):
        return '%s: %s' % ('Container' if self.is_container else 'Media', self.name)


import mptt
mptt.register(Genre)
mptt.register(Theme)
mptt.register(Rubric)
