from rest_framework import serializers
from media.models import Media, Rubric, MediaCountry, Channel



class MediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Media


class RubricSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rubric


class MediaCountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaCountry


class ChannelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Channel
