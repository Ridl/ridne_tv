from rest_framework import viewsets
from media.serializers import MediaSerializer, RubricSerializer, MediaCountrySerializer, ChannelSerializer
from media.models import Media, Rubric, MediaCountry, Channel


class MediaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = Media.objects.all()
    serializer_class = MediaSerializer

    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `username` query parameter in the URL.
        """
        queryset = Media.objects.all()

        print self.pagination_class
        year = self.request.query_params.get('year', None)
        if year is not None:
            queryset = queryset.filter(production_year=int(year))
        return queryset


class RubricViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Rubric.objects.all()
    serializer_class = RubricSerializer

    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `username` query parameter in the URL.
        """
        queryset = Rubric.objects.all()

        parent = self.request.query_params.get('parent', None)
        if parent is not None:
            queryset = queryset.filter(parent=int(parent))
        return queryset


class MediaCountryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = MediaCountry.objects.all()
    serializer_class = MediaCountrySerializer


class ChannelViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Channel.objects.all()
    serializer_class = ChannelSerializer

